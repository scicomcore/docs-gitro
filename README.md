# An introduction to Git

Presentation by Dominic Ward for The Scientific Computing Department at RSCH

The site is served on [BitBucket](https://scicomcore.bitbucket.io/gitro).

## Code

```
git checkout demo_code
```

and `cd ./demo`

OThere are separate directories for Jason and Tina (the two simulated BitBucket
users) where you will find a `demo-cmds.xsh` script with all `git` commands
used.

[doitlive](https://github.com/sloria/doitlive) was used to replay the commands
in the scripts, but you can just take a look at the shell scripts.
